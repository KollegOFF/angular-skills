﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AngularSkills.Backend.Models;

namespace AngularSkills.Backend
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<UserSkill> UserSkills { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(u => u.Login).IsUnique();
            modelBuilder.Entity<UserSkill>().HasKey(us => new { us.RaterId, us.SkillId });

            modelBuilder.Entity<Profile>().HasData(new[]
            {
                new Profile() { Id = 1, Name = "Admin", City = "Amsterdam" },
                new Profile() { Id = 2, Name = "User", City = "Johannesburg" },
            });

            modelBuilder.Entity<User>().HasData(new[]
            {
                new User() { Id = 1, Login = "admin", Password = "admin", ProfileId = 1 },
                new User() { Id = 2, Login = "user", Password = "user", ProfileId = 2 },
            });

            modelBuilder.Entity<Skill>().HasData(new[]
            {
                new Skill() { Id = 1, Name = "Back-end" },
                new Skill() { Id = 2, Name = "Front-end" },
            });

            modelBuilder.Entity<UserSkill>().HasData(new[]
            {
                new UserSkill() { Rating = 5, OwnerId = 1, RaterId = 1, SkillId = 1 },
                new UserSkill() { Rating = 0, OwnerId = 2, RaterId = 1, SkillId = 2 },
                new UserSkill() { Rating = 5, OwnerId = 2, RaterId = 2, SkillId = 2 },
                new UserSkill() { Rating = 0, OwnerId = 1, RaterId = 2, SkillId = 1 },
            });
        }
    }
}
