﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AngularSkills.Backend.Models
{
    public class UserSkill
    {
        [Range(0, 5)]
        public int Rating { get; set; }

        public int? OwnerId { get; set; }

        public Profile Owner { get; set; }

        public int RaterId { get; set; }

        [Required]
        public User Rater { get; set; }

        public int SkillId { get; set; }

        [Required]
        public Skill Skill { get; set; }
    }
}
