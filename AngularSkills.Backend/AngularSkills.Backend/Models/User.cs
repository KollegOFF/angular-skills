﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AngularSkills.Backend.Models
{
    public class User
    {
        public int Id { get; set; }

        [MinLength(2)]
        [MaxLength(50)]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        public int ProfileId { get; set; }

        [Required]
        public Profile Profile { get; set; }
    }
}
