﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AngularSkills.Backend.Models;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;

namespace AngularSkills.Backend.Controllers
{
    public class AccountController : ControllerBase
    {
        private readonly ApplicationContext db;

        public AccountController(ApplicationContext context)
        {
            db = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpPost("/token")]
        public async Task<ActionResult> Token([FromBody] User user)
        {
            if (string.IsNullOrWhiteSpace(user?.Login) || string.IsNullOrWhiteSpace(user?.Password))
            {
                return BadRequest(ModelState);
            }

            User registeredUser = await db.Users.Include(u => u.Profile).FirstOrDefaultAsync(u => u.Login == user.Login && u.Password == user.Password);

            if (registeredUser == null)
            {
                return BadRequest(user);
            }

            var token = new JwtSecurityToken(null, null, null, DateTime.Now, DateTime.Now.AddHours(1), null);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                username = registeredUser.Profile.Name,
            });
        }

        [HttpPost("/register")]
        public async Task<ActionResult> Register([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await db.Users.CountAsync(u => u.Login == user.Login) > 0)
            {
                return Conflict(user);
            }

            db.Users.Add(user);
            await db.SaveChangesAsync();

            return Created($"profiles/{user.ProfileId}", user.Profile);
        }
    }
}
