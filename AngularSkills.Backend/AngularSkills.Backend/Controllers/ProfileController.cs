﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AngularSkills.Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace AngularSkills.Backend.Controllers
{
    [Route("api/profiles")]
    public class ProfileController : ControllerBase
    {
        private readonly ApplicationContext db;

        public ProfileController(ApplicationContext context)
        {
            db = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet("{query?}")]
        public async Task<ActionResult<Profile[]>> GetProfiles(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return await db.Profiles.ToArrayAsync();
            }

            return await db.Profiles
                .Include(p => p.Skills)
                .Where(p =>
                    p.Name.Contains(query) ||
                    p.City.Contains(query) ||
                    p.Skills.Any(s => s.Skill.Name.Contains(query) || s.Skill.Description.Contains(query))
                )
                .ToArrayAsync();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Profile>> GetProfile(int id)
        {
            Profile profile = await db.Profiles.Include(p => p.Skills).FirstOrDefaultAsync(p => p.Id == id);
            if (profile == null)
            {
                return NotFound();
            }

            return profile;
        }

        [Authorize]
        [HttpPut("{id:int}")]
        public async Task<ActionResult> PutProfile(int id, Profile profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = await db.Users.Include(u => u.Profile).FirstOrDefaultAsync(u => u.Login == User.Identity.Name);
            if (user == null || id != user.Profile.Id || id != profile.Id)
            {
                return BadRequest();
            }

            db.Entry(profile).State = EntityState.Modified;

            await db.SaveChangesAsync();

            return NoContent();
        }
    }
}
