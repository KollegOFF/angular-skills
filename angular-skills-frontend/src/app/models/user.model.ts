import { Profile } from './profile.model';

export class User {
  public login: string;
  public password: string;
  public profile: Profile;

  constructor(login: string, password: string, username?: string) {
    this.login = login;
    this.password = password;
    if (username) {
      const profile: Profile = new Profile();
      profile.name = username;

      this.profile = profile;
    }
  }
}
