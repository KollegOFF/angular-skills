import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  private host = 'http://localhost:21785';

  private namespace = 'api';

  get url(): string {
    return `${this.host}/${this.namespace}`;
  }

  constructor(private http: HttpClient) { }

  getProfiles(query?: string) {
    return this.http.get(`${this.url}/profiles${query ? `?query=${query}` : '' }`);
  }
}
