import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private host = 'http://localhost:21785';

  token: string;

  username: string;

  constructor(private http: HttpClient) { }

  signin(user: User) {
    const request = this.http.post(`${this.host}/token`, user, { observe: 'response' });

    request.subscribe((response: HttpResponse<any>) => {
      this.token = response.body.token;
      this.username = response.body.username;
      console.log(response.body);
    });

    return request;
  }

  signup(user: User) {
    return this.http.post(`${this.host}/register`, user);
  }

  logout() {
    this.token = null;
    this.username = null;
  }
}
