import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Profile } from '../models/profile.model';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css'],
  providers: [DataService],
})
export class ProfilesComponent implements OnInit {
  profiles: Profile[];

  query: string;

  loaded = false;

  constructor (private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getProfiles().subscribe((profiles: Profile[]) => {
      this.profiles = profiles;
      this.loaded = true;
    });
  }

  search() {
    this.loaded = false;
    this.dataService.getProfiles(this.query).subscribe((profiles: Profile[]) => {
      this.profiles = profiles;
      this.loaded = true;
    });
  }

}
