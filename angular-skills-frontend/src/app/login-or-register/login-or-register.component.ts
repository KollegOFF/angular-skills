import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';
import { User } from '../models/user.model';
import { Profile } from '../models/profile.model';

@Component({
  selector: 'app-login-or-register',
  templateUrl: './login-or-register.component.html',
  styleUrls: ['./login-or-register.component.css'],
})
export class LoginOrRegisterComponent {
  loading = false;

  isLinks: boolean;

  isLogin: boolean;

  isRegister: boolean;

  name: string;

  login: string;

  password: string;

  get username(): string {
    return this.authService.username;
  }

  constructor(private authService: AuthService, private router: Router) {
    this.isLinks = router.url === '/';
    this.isLogin = router.url === '/login';
    this.isRegister = router.url === '/register';
  }

  signin() {
    this.loading = true;
    const user: User = new User(this.login, this.password);
    this.authService.signin(user).subscribe(() => {
      this.router.navigate(['/']);
    }, () => {
      this.reset();
    });
  }

  signup() {
    this.loading = true;
    const user: User = new User(this.login, this.password, this.name);
    this.authService.signup(user).subscribe(() => {
      this.router.navigate(['login']);
    }, () => {
      this.reset();
    });
  }

  logout() {
    this.authService.logout();
  }

  private reset() {
    this.name = null;
    this.login = null;
    this.password = null;
    this.loading = false;
  }

}
