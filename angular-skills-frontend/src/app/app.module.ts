import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { LoginOrRegisterComponent } from './login-or-register/login-or-register.component';
import { AuthService } from './auth.service';

const routes: Routes = [
  { path: '', component: ProfilesComponent },
  { path: 'login', component: LoginOrRegisterComponent },
  { path: 'register', component: LoginOrRegisterComponent },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  declarations: [
    AppComponent,
    ProfilesComponent,
    LoginOrRegisterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
